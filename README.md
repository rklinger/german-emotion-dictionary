# Introduction #

In this repository, dictionaries for German emotion analysis are available. Currently, we are focussing on the development for seven fundamental emotions following roughly the [theory by Paul Ekman](http://www.paulekman.com/wp-content/uploads/2013/07/Basic-Emotions.pdf). Some information on different classification schemes of emotions can be found [here](http://www.theemotionmachine.com/classification-of-emotions). These resources cannot be complete. However, they provide a seed set for automatically generating more comprehensive lists for specific domains. If you like to add specific words to these dictionaries, pull requests are appreciated. For specific use-cases, the choice of emotions might be sub-optimal. We are happy to discuss novel applications with you!

These dictionaries are under current development and evaluation to provide a resource for German emotion analysis in different areas. If you have questions, please contact [Roman Klinger](http://www.romanklinger.de/).

People involved in generation of these resources are:

* [Roman Klinger](http://www.romanklinger.de/)
* Surayya Samat Suliya

# Resources #

Resources which have been used to compile these dictionaries:

* [GermanPolarityClues](http://www.ulliwaltinger.de/sentiment/)
* [OpenThesaurus](https://www.openthesaurus.de/about/download)
* [SentiWS](http://wortschatz.uni- leipzig.de/download/sentiws.html)
* [SentiWordNet](http://sentiwordnet.isti.cnr.it)
* [NRC Emotion Lexicon](http://www.purl.org/net/NRCemotionlexicon)

# Software #

In addition, this repository contains some small scripts to generate depictions with the dictionaries based on input text.

## Installation ##

This software is not prepared to be used without prior knowledge of a Unix/Linux/OS X command line. We plan to develop a user friendly interactive interface in the future.

Prerquisites for installation are the following (We tested it on Mac OS X 10.10.5 and Fedora Linux release 23.):

* Java 1.8 (if are on 1.7, you need to recompile, see below)
* Gnuplot (with pdfcairo included, e.g. on a Mac installed with [homebrew](http://brew.sh) and `brew install gnuplot --wx --cairo --pdf --with-x --tutorial`)
* Maven (if you want to compile)

To install these small pieces of software, first check out the repository (the last part of this line is the name for the folder on your local harddisk):

	git clone https://bitbucket.org/rklinger/german-emotion-dictionary.git german-emotion-dictionary
	
Change to the directory you just created:

	cd german-emotion-dictionary
	
Now you can run the analysis for tracking emotions over time:

	./bin/plot.sh data/schloss.txt
	
The file `out/schloss/show.pdf` should show the result now.

You can also run the character profiling:

	./bin/profile.sh data/schloss.txt 0.00005
	
If you change the code and want to recompile, you can do that with Maven (however, this is not necessary, as the target directory of this project contains the compiled version):

	mvn compile assembly:single

If you are on Java 1.7, change the `pom.xml` to scala version 2.10.0.
	


# License #

These resources are made available under the [Lesser GNU Public License (LGPL)](http://creativecommons.org/licenses/LGPL/2.1/).

# Publications #

* Roman Klinger, Surayya Samat Suliya, and Nils Reiter.
 Automatic Emotion Detection for Quantitative Literary Studies – A
  case study based on Franz Kafka's Das Schloss und Amerika.
 In *Digital Humanities (DH)*, Kraków, Poland, 2016. [[bib](http://www.romanklinger.de/publications/2016_bib.html#Klinger2016)] [[pdf](http://www.romanklinger.de/publications/klinger-samat-reiter2016.pdf)]