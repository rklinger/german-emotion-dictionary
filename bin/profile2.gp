set terminal pdfcairo size 10cm,10cm font ",20"
set output pdffile
set title thetitle

unset title

#max=0.00004

unset border
set polar
set angles degrees

# GRID
#set style line  lt 2 lc 0 lw 0.3 #redefine a new line style for the grid
set grid polar 60 #set the grid to be displayed every 60 degrees
set grid ls 0

#set xrange [-0.4:0.4]
#set yrange [-0.4:0.4]

#set xrange [-0.4:0.4]
#set yrange [-0.4:0.4]

set xrange [-max:max]
set yrange [-max:max]

unset xtics
unset ytics
unset raxis
set rtics 0,max/2
set rtics format " "

labelpos=0.3
labelpos=max
set size square 
set key below
#this places a label on the outside
set_label(x, text) = sprintf("set label '%s' at (labelpos*cos(%f)), (labelpos*sin(%f))     center", text, x, x) 
xvalue(x) = (0.3*cos(x))
yvalue(x) = (0.3*sin(x))

#plot datafile u (0.3*cos($2)):(0.3*sin($2)):1 w labels notitle
#plot datafile u (yvalue($2)):(xvalue($2)):1 w labels center notitle

eval set_label(0, "Ekel")
eval set_label(60, "Freude")
eval set_label(120, "Furcht")
eval set_label(180, "Trauer")
eval set_label(240, "Ueberraschung")
eval set_label(300, "Wut")


plot for [i=3:numcolumn] datafile u 2:i t columnheader(i) w lp pt 7

do for [i=3:numcolumn] {
   plot datafile u 2:i t columnheader(i) w lp pt 7 lw 10 lc rgb "blue"
}
