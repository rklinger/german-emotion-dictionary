set terminal pdfcairo size 15cm,30cm
set output pdffile
set key autotitle columnhead

set tmargin 1
set bmargin 5
set lmargin 3
set rmargin 3
unset xtics
unset ytics

set multiplot layout 7,1 title thetitle font ",20"

#set key autotitle column nobox samplen 1 noenhanced

plot [][:max1]splitfile using ($1):(0):(min1+0.001):(max1-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:2 w l lw 10 lc rgb "cyan"
plot [][:max2]splitfile using ($1):(0):(min2+0.001):(max2-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:3 w l lw 10 lc rgb "red"
plot [][:max3]splitfile using ($1):(min3+0.001):(0):(max3-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:4 w l lw 10 lc rgb "plum"
plot [][:max4]splitfile using ($1):(min4+0.001):(0):(max4-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:5 w l lw 10 lc rgb "brown"
plot [][:max5]splitfile using ($1):(min5+0.001):(0):(max5-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:6 w l lw 10 lc rgb "violet"
plot [][:max6]splitfile using ($1):(min6+0.001):(0):(max6-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:7 w l lw 10 lc rgb "blueviolet"
plot [][:max7]splitfile using ($1):(min7+0.001):(0):(max7-0.001) w vectors nohead lc rgb "grey" lw 5, datafile u 1:8 w l lw 10 lc rgb "olive"

unset multiplot
