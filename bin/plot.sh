#!/bin/bash
if [[ "$1" == ""  ]] ; then echo "Provide a parameter!" ; exit ; fi
NOVEL=$1
BASE=`dirname $0`/..
DICTFOLDER=`pwd`/$BASE/fundamental

#mvn compile assembly:single

DATAFOLDER=$BASE/out/`basename "$1" .txt`
PDFFILE=$DATAFOLDER/show.pdf
mkdir -p "$DATAFOLDER"
# 17 for amerika
# 30 for schloss?
WINDOWSIZE=20
STEPSIZE=5
if [[ "$2" == ""  ]] ; then
    echo "Default window size of $WINDOWSIZE is used!" ;
else WINDOWSIZE=$2 ;
fi


java -Xmx2g -jar $BASE/target/recognition-0.1-jar-with-dependencies.jar "$DICTFOLDER" "$NOVEL" "$DATAFOLDER/list.data" "$DATAFOLDER/splitpoints.data" $WINDOWSIZE $STEPSIZE

MAX1=`cut -f 2 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX2=`cut -f 3 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX3=`cut -f 4 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX4=`cut -f 5 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX5=`cut -f 6 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX6=`cut -f 7 "$DATAFOLDER/list.data" | sort -g | tail -1`
MAX7=`cut -f 8 "$DATAFOLDER/list.data" | sort -g | tail -1`
MIN1=`cut -f 2 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN2=`cut -f 3 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN3=`cut -f 4 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN4=`cut -f 5 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN5=`cut -f 6 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN6=`cut -f 7 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`
MIN7=`cut -f 8 "$DATAFOLDER/list.data" | sort -g -r | tail -2 | head -1`

#echo "$MIN3 $MAX3"

gnuplot \
-e "max1='$MAX1'" \
-e "max2='$MAX2'" \
-e "max3='$MAX3'" \
-e "max4='$MAX4'" \
-e "max5='$MAX5'" \
-e "max6='$MAX6'" \
-e "max7='$MAX7'" \
-e "min1='$MIN1'" \
-e "min2='$MIN2'" \
-e "min3='$MIN3'" \
-e "min4='$MIN4'" \
-e "min5='$MIN5'" \
-e "min6='$MIN6'" \
-e "min7='$MIN7'" \
-e "thetitle='`basename $NOVEL .txt` $WINDOWSIZE $STEPSIZE'" \
-e "pdffile='$PDFFILE'" \
-e "datafile='$DATAFOLDER/list.data'" \
-e "splitfile='$DATAFOLDER/splitpoints.data'" \
$BASE/bin/plot2.gp
