set terminal pdfcairo size 20cm,10cm
set output pdffile
set key autotitle columnhead
set title thetitle
plot splitfile using ($1):(0):(0):(0.14) w vectors nohead lc rgb "grey" lw 5,\
     datafile u 1:2 w l lw 5,\
           "" u 1:3 w l lw 5,\
	   "" u 1:4 w l lw 5,\
	   "" u 1:5 w l lw 5,\
	   "" u 1:6 w l lw 5,\
	   "" u 1:7 w l lw 5,\
	   "" u 1:8 w l lw 5,\

