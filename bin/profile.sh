#!/bin/bash
if [[ "$1" == ""  ]] ; then echo "Provide a text file!" ; exit ; fi
if [[ "$2" == ""  ]] ; then echo "Provide a max file. Try 0.0004!" ; exit ; fi
NOVEL=$1
ENTITYFILE=`dirname $NOVEL`/`basename $NOVEL .txt`-entities.csv
BASE=`dirname $0`/..
DICTFOLDER=`pwd`/$BASE/fundamental
MAX=$2

# mvn compile assembly:single

DATAFOLDER=$BASE/out/`basename "$1" .txt`
PDFFILE=$DATAFOLDER/profile.pdf
mkdir -p "$DATAFOLDER"
WINDOWSIZE=20
java -Xmx2g -cp $BASE/target/recognition-0.1-jar-with-dependencies.jar ims.emotion.Profiler "$DICTFOLDER" "$NOVEL" "$ENTITYFILE" "$DATAFOLDER/profile.data" $WINDOWSIZE

#MAX=`cut -f 2 "$DATAFOLDER/list.data" | sort -g | tail -1`
NUMCOLUMN=`awk 'NR==1 {print NF}' $DATAFOLDER/profile.data`

gnuplot \
-e "max='$MAX'" \
-e "thetitle='`basename $NOVEL .txt`'" \
-e "pdffile='$PDFFILE'" \
-e "datafile='$DATAFOLDER/profile.data'" \
-e "splitfile='$DATAFOLDER/splitpoints.data'" \
-e "numcolumn='$NUMCOLUMN'" \
$BASE/bin/profile2.gp
