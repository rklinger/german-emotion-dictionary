package ims.emotion

import java.io.{PrintStream, File}
import java.util
import java.util.StringTokenizer

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import scala.io.Source
import org.tartarus.snowball.ext.German2Stemmer

/**
 * Created by rklinger on 1/19/16.
 */
class Recognize {
  val germanStemmer = new German2Stemmer()
  val dictionaries = new mutable.HashMap[String, mutable.HashSet[String]]
  val dictKeys = new ArrayBuffer[String]
  val negativeList = new mutable.HashSet[String]
  negativeList += "schon"
  negativeList += "zogen"
  negativeList += "zog"
  negativeList += "anders"
  val stemMapping = new mutable.HashMap[String,mutable.HashSet[String]]

  
  def readDictionaries(dicFolder: String, omitVerachtung:Boolean) = {
    System.err.println("Reading dictionaries...")
    for (dict <- new File(dicFolder).listFiles ; if (!(omitVerachtung && dict.getName.equals("Verachtung.txt")))) { // no Verachtung in the moment
      System.err.println("Reading "+dict)
      val dictName = dict.getName.replaceAll(".txt", "")
      dictKeys += dictName
      for (line <- Source.fromFile(dict,"ISO-8859-1").getLines()) {
        //System.err.println("Reading "+line+" to "+tokenizeAndStem(line).mkString(":"))
        if (!dictionaries.contains(dictName)) dictionaries += (dictName -> new mutable.HashSet[String])
        tokenizeAndStem(line).foreach(token => {if (!negativeList.contains(token)) dictionaries(dictName) += token})
      }
    }
    System.err.println("Read dictionaries: ")
    dictKeys.foreach(key => {
      System.err.println(key+":\t"+dictionaries(key).size+" entries")
    })
  }

  def tokenizeAndStem(string: String,nostem:Boolean=false): Array[String] = {
    //System.err.println("RUN")
    val st = new StringTokenizer(string);
    val tokens = new mutable.ArrayBuffer[String]
    while (st.hasMoreTokens()) {
      val nextToken = st.nextToken().replaceAll("[^A-Za-z]","")
      //val old = nextToken
      //if (nextToken.startsWith("There")) System.err.print("XXX "+nextToken)
      if (!nostem) {
        germanStemmer.setCurrent(nextToken)
        if (germanStemmer.stem()) {
          val currentStem = germanStemmer.getCurrent
          tokens += currentStem
          if (!stemMapping.contains(currentStem)) stemMapping += currentStem -> new mutable.HashSet[String]
          stemMapping(currentStem).add(nextToken)
        }
      } else {
        tokens += nextToken
        if (!stemMapping.contains(nextToken)) stemMapping += nextToken -> new mutable.HashSet[String]
        stemMapping(nextToken).add(nextToken)
      }
      //if (nextToken.startsWith("and")) println(">>> "+old+"   "+nextToken)
      //if (nextToken.startsWith("There")) System.err.println(" -> "+tokens.last)
    }
    tokens.toArray
  }
  
  def printFrequentWordInSlices(stemmedTokens:List[Array[String]], dictionaries:mutable.HashMap[String,mutable.HashSet[String]]) = {
    for (chunk <- stemmedTokens) {
      System.err.println("Next Chunk")
      for (key <- dictKeys) {
        System.err.println("\t"+key)
        val emotionWordCountPairs = dictionaries(key).map(emotionWord => (emotionWord,chunk.count(token => token == emotionWord))) // SUPERSLOW!
        System.err.println(emotionWordCountPairs.toList.sortBy(x => -x._2).slice(0,10).map(x => "\t\t"+stemMapping(x._1).mkString("(",";",")")+"\t"+x._2).mkString("\n"))
      }
    }
  }
  
  def countDictInWindows(stemmedTokens: Array[String], dictionary:mutable.HashSet[String], windowSize:Integer, stepSize:Integer) : Array[Double] = {
    val counts = new ArrayBuffer[Int]
    for(slice <- stemmedTokens.sliding(windowSize,stepSize)) {
      counts += slice.count(token => dictionary.contains(token))
    }
    counts.toArray.map(c => c.toDouble/dictionary.size.toDouble)
  }
  
  def prepareNovel(novel:String, splitString:String,nostem:Boolean=false) : List[Array[String]] = {
    val novelAsString = Source.fromFile(novel,"ISO-8859-1").getLines().mkString(" ")
    if (!novelAsString.contains(splitString)) {
      val tokensForWholeNovel = tokenizeAndStem(novelAsString,nostem)
      val r = List(tokensForWholeNovel)
      r
    }
    else {
      val tokenInChunks = new ArrayBuffer[Array[String]]
      val chunks = novelAsString.split(splitString)
      //chunks.foreach(x => println("CHUNK!\n"+x+"\n\n\n\n")) // DEBUG
      System.err.println("Length of chunks:"+" "+chunks.map(c => c.length).mkString("["," ; ","]"))
      chunks.map(chunk => tokenizeAndStem(chunk,nostem)).toList
    }
  }
  
  def count(stemmedTokens:List[Array[String]], dictionaries:mutable.HashMap[String,mutable.HashSet[String]], windowSize:Int, stepSize:Int, countps:PrintStream, splitPointPS:PrintStream) = {
    val counts = new mutable.HashMap[String,ArrayBuffer[Double]]
    for (chunk <- stemmedTokens) {
      for (key <- dictKeys) {
        if (!counts.contains(key)) counts += (key -> new ArrayBuffer[Double])
        val r = countDictInWindows(chunk, dictionaries(key), windowSize, stepSize)
        counts(key) ++= r
      }
      splitPointPS.println(counts(dictKeys.head).length*stepSize)
    }
    countps.println("id\t"+dictKeys.mkString("\t"))
//    counts.keySet.foreach(k => print(counts(k).size+" ")) // this needs to be true!
    for(i <- 0 until counts(dictKeys.head).length)
      countps.println((i*stepSize)+"\t"+dictKeys.map(key => /*key+":"+*/counts(key)(i)).mkString("\t"))
  }

  /**
   * Prepare List of entity synonyms, separated by tab, first being the canonical name
   * The data structure is: canonical name -> set of tokenized/stemmed entries, each in an array
   * @param entityMentionListFile
   * @return
   */
  def readEntityMentionList(entityMentionListFile:String) : mutable.HashMap[String,Set[Array[String]]] = {
    System.err.println("Reading entities from file "+entityMentionListFile)
    val entityLists = new mutable.HashMap[String,Set[Array[String]]]
    for(line <- Source.fromFile(entityMentionListFile,"ISO-8859-1").getLines()) {
      val s = line.split('\t')
      entityLists += s(0) -> s.slice(1,s.length).toSet.map((tokens:String) => tokenizeAndStem(tokens,nostem=true))
    }
    System.err.println(entityLists.keys.toList.sortBy(x => x).map(key => key+" => "+entityLists(key).map(set => set.mkString("[",":","]"))))
    entityLists
  }
  
  def indicesOfSlices(tokens:Array[String],slice:Array[String]) : Array[Int] = {
    var nextIndex = 0
    val indices = new ArrayBuffer[Int]
    //System.err.println
    //System.err.println("!Check: "+slice.mkString(":"))
    //System.err.println(tokens.mkString("\n"))
    while (nextIndex != -1) {
      val index = tokens.indexOfSlice(slice,nextIndex+1)
      nextIndex = index
      if (index != -1) indices += index
      //System.err.print(index+":")
    }
    //System.err.println
    indices.toArray
  }
  
  /**
   * Counting in the environment of specific terms.
   */
  def profile(tokens:List[Array[String]], stemmedTokens:List[Array[String]], windowSize:Int, countps:PrintStream, entityLists:mutable.HashMap[String,Set[Array[String]]]) = {
    val canonicalentities = entityLists.keys.toList.sortBy(x => x)
    // to count: (entity,emotion) -> count
    val counts = new mutable.HashMap[(String,String),Double]
    val numOfEntityMentions = new mutable.HashMap[(String),Int]

    // for each entity of interest
    for (entityCanonicalName <- canonicalentities){
      System.err.println("Searching for: "+entityCanonicalName)

      // initialization of counter for pairs of entity and emotion
      dictKeys.foreach(emotion => counts += (entityCanonicalName,emotion) -> 0.0)
      numOfEntityMentions += entityCanonicalName -> 0
      
      // for each name which belongs to a canonical name
      for(entityNameTokens <- entityLists(entityCanonicalName)) {
        //System.err.println("\tsynonym: "+entityNameTokens.mkString("[",":","]"))
        
        // for each chunk in the novel
        for (chunkIndex <- 0 until tokens.length) {
          val relevantIndices = indicesOfSlices(tokens(chunkIndex),entityNameTokens)
          //System.err.println("\t\tfound: "+relevantIndices.mkString("[",":","]"))
          numOfEntityMentions += entityCanonicalName -> {numOfEntityMentions(entityCanonicalName)+relevantIndices.size}
          for (relevantIndex <- relevantIndices) {
            // counting the emotions
            for (emotion <- dictKeys) {
              val windowAroundEntity = stemmedTokens(chunkIndex).slice(scala.math.max(relevantIndex-windowSize,0),scala.math.min(tokens(chunkIndex).length,relevantIndex+windowSize))
              val r = windowAroundEntity.count(token => { val r = dictionaries(emotion).contains(token) ; if (r) System.err.println("EmotionCharacterWords\t"+entityCanonicalName+"\t"+emotion+"\t"+stemMapping(token).mkString(";")) ; r })
              //System.err.println(relevantIndex+"\t"+chunk(relevantIndex)+"\t"+emotion+"\t"+r)
              val oldCount = counts((entityCanonicalName,emotion))
              val newCount = r/windowAroundEntity.size.toDouble
              counts += {(entityCanonicalName,emotion) -> {oldCount + newCount}}
            }
          }
        }
      }
     System.err.println("Number found: "+entityCanonicalName+" is "+numOfEntityMentions(entityCanonicalName))
    }
    
    // renormalize each entity
    for (entity <- canonicalentities) {
      println(entity)

      // by dictionary size
      for (emotion <- dictKeys) counts += (entity,emotion) -> { (counts((entity, emotion))) / (dictionaries(emotion).size.toDouble) }
      //println((for (emotion <- dictKeys) yield counts(entity,emotion)))
      // by mention number      
      for (emotion <- dictKeys) counts += (entity,emotion) -> { (counts((entity, emotion))) / (numOfEntityMentions(entity).toDouble) }
      //println((for (emotion <- dictKeys) yield counts(entity,emotion)))
      // to sum to one
      //val sum = (for (emotion <- dictKeys) yield counts((entity,emotion))).sum
      //for (emotion <- dictKeys) counts += (entity,emotion) -> { (counts((entity, emotion))) / (sum) }
      //println((for (emotion <- dictKeys) yield counts(entity,emotion)))
      // log transformation?
      //for (emotion <- dictKeys) counts += (entity,emotion) -> { scala.math.log10(counts((entity, emotion))) }
    }

    //        counts += (entity,emotion) -> { (old-min) / (max-min) }
    
    // print
    countps.println(canonicalentities.mkString("Emotion\tDegree\t","\t",""))
    var degree = 0
    for(emotion <- dictKeys) {
      countps.print(emotion+"\t"+degree+"\t")
      degree += 60
      for(entity <- canonicalentities) {
        countps.print(counts((entity,emotion))+"\t")
      }
      countps.println
    }
  }
}

object Recognizer {
  def main (args: Array[String]) {
    System.err.println("IMSER (IMS Emotion Recognizer) (klinger@ims.uni-stuttgart.de)")
    System.err.println("(LGPL)")
    System.err.println()
    if (args.length < 2) {
      System.err.println("Parameters: folderWithTxtFiles textFile countFile splitPointFile windowSizeFactor stepSize")
      System.exit(1)
    }
    val folderWithTxtFiles = args(0)
    val novel = args(1)
    val r = new Recognize
    val windowSizeFactor = if (args.length > 4) args(4).toDouble else 10.toDouble
    val stepSize = if (args.length > 4) args(5).toInt else 10
    System.err.println("Reading text to be analyzed from "+novel)
    //val novelTokens = r.tokenizeAndStem(Source.fromFile(novel).getLines().mkString(" "))
    val novelTokens = r.prepareNovel(novel,"@@@splitpoint@@@")
    val lengthOfWholeNovel = novelTokens.map(_.length).sum
    val windowSize = ((lengthOfWholeNovel.toDouble/windowSizeFactor.toDouble)).round.toInt // novelTokens.length.toDouble
    val countPS = if (args.length > 2) new PrintStream(new File(args(2))) else System.err
    val splitpointPS = if (args.length > 3) new PrintStream(new File(args(3))) else new PrintStream(new File("/dev/null"))

    System.err.println("length Of whole novel: "+lengthOfWholeNovel)
    System.err.println("step size: "+stepSize)
    System.err.println("==> window size: "+windowSize)

    r.readDictionaries(folderWithTxtFiles,false)

    r.count(novelTokens, r.dictionaries, windowSize, stepSize, countPS, splitpointPS)
    
    r.printFrequentWordInSlices(novelTokens,r.dictionaries)
  }
}

object filterEmotionVsStopwords {
  def main (args: Array[String]) {
    val r = new Recognize
    r.readDictionaries("fundamental",false)
    val alldic = new mutable.HashSet[String]()
    alldic ++= r.dictionaries.map(x => x._2).flatten

    val stopwordlines = Source.fromFile("suppl/some-neutral-stopwords.txt","ISO-8859-1").getLines()
    println(stopwordlines.filter(stopword => alldic.contains(r.tokenizeAndStem(stopword).head)).mkString("\n"))

  }
  
}