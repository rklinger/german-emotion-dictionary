package ims.emotion

import java.io.{File, PrintStream}

import scala.collection.mutable

/**
 * Created by rklinger on 2/11/16.
 */

object Profiler {
  def main (args: Array[String]) {

    
    
    System.err.println("IMSER (IMS Emotion Profiler) (klinger@ims.uni-stuttgart.de)")
    System.err.println("(LGPL)")
    System.err.println()
    if (args.length < 2) {
      System.err.println("Parameters: folderWithTxtFiles textFile entityMentionListFile countOutFile windowSize")
      System.exit(1)
    }
    val folderWithTxtFiles = args(0)
    val novel = args(1)
    val entityList = args(2)
    val r = new Recognize
    val windowSize = if (args.length > 4) args(4).toInt else 1000
    System.err.println("Reading text to be analyzed from "+novel)

    val novelTokensStemmed = r.prepareNovel(novel,"@@@splitpoint@@@",nostem=false)
    val novelTokensNotStemmed = r.prepareNovel(novel,"@@@splitpoint@@@",nostem=true)
    //System.out.println((for(chunks <- novelTokensStemmed ; tokens <- chunks) yield tokens).mkString("\n"))
    //System.exit(1)
    val lengthOfWholeNovel = novelTokensStemmed.map(_.length).sum
    val countPS = if (args.length > 3) new PrintStream(new File(args(3))) else System.out

    System.err.println("length Of whole novel: "+novelTokensNotStemmed.map(_.length).sum)
    System.err.println("length Of whole stemmed novel: "+novelTokensStemmed.map(_.length).sum)
    System.err.println("==> window size: "+windowSize)

    r.readDictionaries(folderWithTxtFiles,true)
    val entityLists = r.readEntityMentionList(entityList)
    //r.count(novelTokens, r.dictionaries, windowSize, stepSize, countPS, splitpointPS)
    r.profile(novelTokensNotStemmed,novelTokensStemmed,windowSize,countPS,entityLists)
  }
}